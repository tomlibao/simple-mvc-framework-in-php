<?php
namespace app\controllers;

use core\base\Controller;

class SchoolController extends Controller
{
    public function demo(){
        $this->assign('username', '李富贵');
        $this->assign('age', '40');
        $this->assign('sex', '男');
        $this->render();
    }
}