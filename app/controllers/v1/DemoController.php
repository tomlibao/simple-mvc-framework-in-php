<?php

namespace app\controllers\v1;

use core\base\Controller;
use core\db\Db;
use core\extend\ArrayHandle;
use core\extend\IdCardHandle;
use core\extend\StringHandle;
use core\extend\TimeHandle;
use core\extend\ValidateHandle;

class DemoController extends Controller
{
    public function demo()
    {
        echo '我是控制器demo';
//        $obj = new TestController();
//        $obj->test();

//        $obj = new StringHandle();
//        $res = $obj->hideSignString(15333885922, '*', 3,  4);
//        echo $res;
//        $obj = new ValidateHandle();
//        $res = $obj->isLegalIp('210.22.245.36');
//        vd($res);
//        $info = Db::query('select * from school');
//        pd($info);
    }

    public function demos(){
        $key = 'iwsojfiowejgiroegnioamr';
        // 接收客户端提交的 token 。
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczovL3d3dy5ubnRvb2wuY2MiLCJpYXQiOjE2Njg3NTY0ODQsImV4cCI6MTY2ODc1NjYwNCwiZGF0YSI6eyJ1c2VyX2lkIjoxLCJ1c2VyX25hbWUiOiJ0b21saWJhbyJ9fQ.xhPZ80nr9nAoeSCa80steMVsNDisTfC8IuUcMQpbYVc';
        $test = JWT::decode($token, new Key($key, 'HS256'));
        pd($test->iss);
    }

    public function route1()
    {
        echo '我是路由分组1';
    }

    public function route2()
    {
        echo '我是路由分组2';
    }
}