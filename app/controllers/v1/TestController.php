<?php
namespace app\controllers\v1;

use app\models\Classroom;
use app\models\School;
use app\models\Student;
use core\auth\Cookie;
use core\auth\Session;
use core\base\Controller;
use core\db\Db;
use core\extend\ArrayHandle;
use core\extend\IdCardHandle;
use core\extend\PersonNameHandle;
use core\extend\StringHandle;
use core\extend\ValidateHandle;
use core\request\Request;
use core\response\Response;

class TestController extends Controller
{

    public function response()
    {
        $data = [
            'username' => 'tomlibao',
            'age' => 20
        ];
        return Response::json($data);
    }

    public function auth()
    {
        Session::set('username', 'tom');
        $res = Session::get('username');

        $res = Cookie::set('hehe', 22);
        eol($res);
        pd(Cookie::get('hehe'));
    }

    public function validate()
    {
        $data = [
            'name' => 1,
            'age' => 29
        ];
        $validate = new \app\validate\Student();
        $result = $validate->scene('add')
            ->batch(true)
            ->check($data);
        pd($result);
    }

    public function test()
    {
//        echo $this->request->get('id');
        echo 'test控制器'."<br/>";
        return 123;
//        $arr = [
//            [
//                'id' => 1,
//                'price' => 10
//            ],[
//                'id' => 2,
//                'price' => 20
//            ],[
//                'id' => 3,
//                'price' => 30
//            ],
//        ];
//        $obj = new ArrayHandle();
//        $res = $obj->getArrFieldSum($arr, 'price');
//        pd($res);
//        $student = new Student;
//        $student->age = 20;
//        $student->sex = 2;
//        $student->save();

//        Student::where()->find();

//        $info = Db::name('sd_ceshi')
//            ->value('id');
//        pd($info);
//        $info = Db::execute('update sd_ceshi set age = 3 where id = 2');
//        pd($info);
//        echo 123;
//        $obj = new StringHandle();
//        echo $obj->removeStringStart('abcdefg');
//        $data = Db::name('sd_ceshi')
//            ->sum('age');
//        pd($data);
//        echo 123;
//        getTest();
//        $fileName = '15666845920';
//        $a = $obj->hideSignString($fileName, '*', 4, 4);
//        print_r($a);
    }




    public function demo()
    {
//        echo date('Y-m-d H:i:s');

//        $class = Classroom::find(1);
//        $data = $class->hasManyStudent();
//        pd($data);


//        $res = Classroom::with('hasOneStudent')
//            ->field('student.id as sid,cname,classroom.id as cid,sname')
//            ->where('student.id','=',1)
//            ->find();
//        pd($res);
//        $student = new Student();
//        $student->age = 50;
//        $student->cid = 2;
//        $student->sname = '玉麒麟卢俊义';
//        $res = $student->save();
//        pd($res);
//        print_r($data->id);
//        $student = Student::find(10);
//        $student->sname = '浪子燕青';
//        $student->age = 45;
//        $student->cid = 1;
//        $res = $student->save();
//        pd($res);
//        pd($student);
//        $data = Db::name('student')->select();
//        print_r($data);

//        $data = Student::order("id", 'desc')
//            ->field('sname,age')
//            ->limit(2)
//            ->select();
//        print_r($data);


//        $student = Student::find(1);
//        echo $student->sname.PHP_EOL;

//        print_r($student);

//        $data = Student::select();
//
//        pd($data);

//        $res = Student::where('id','=', 6)
//            ->delete();
//        pd($res);

//        $student = Student::find(11);
//        $res = $student->delete();
//        pd($res);

//        $res = Student::destroy(10);
//        pd($res);

//        $res = Classroom::where('id', '=', 2)
//            ->find();
//        print_r($res);
//        print_r($data);
//        echo date('Y-m-d H:i:s');
    }


    public function haha()
    {
//        echo time();
        //一对一
//        $class = Student::find(1);
//        $data = $class->hasOneClassroom();
//        pd($data);

        //一对一 with
//        $res = Student::with('hasOneClassroom')
//            ->field('student.id as sid,cname,classroom.id as cid,sname')
//            ->where('student.id','=',1)
//            ->find();
//        pd($res);


        //一对多
//        $class = Classroom::find(1);
//        $data = $class->hasManyStudent();
//        pd($data);


        //一对多 with
//        $res = Classroom::with('hasManyStudent')
//            ->field('student.id as sid,cname,classroom.id as cid,sname')
//            ->where('classroom.id','=',1)
//            ->select();
//        pd($res);


        //多对多 with
//        $res = Classroom::with('hasManyStudent')
//            ->field('student.id as sid,cname,classroom.id as cid,sname')
////            ->where('student.id','=',1)
//            ->select();
//        pd($res);


        //远程一对多
//        $class = School::find(1);
//        $res = $class->hasManyClass();
//        pd($res);

        //远程多对多 with
        $res = School::with('hasManyClass')
            ->field('school.school_name, address, cname, sname')
            ->select();

        print_r($res);

//        echo time();

    }
}