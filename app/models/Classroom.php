<?php

namespace app\models;

use core\base\Model;

class Classroom extends Model
{
    protected $tables = 'classroom';

    public function __construct()
    {
        parent::__construct($this->tables);
    }

//    public function getCnameAttr($cname)
//    {
////        return $cname;
//        return '吃饭的光头强';
//    }

    public function hasManyStudent()
    {
        return $this->hasMany(Student::class, 'cid');
    }

    public function hasOneStudent()
    {
        return $this->hasOne(Student::class, 'cid');
    }

//    public function test()
//    {
//        $res = self::with('hasManyStudent')
//            ->field('student.id as sid,cname,classroom.id as cid')
//            ->select();
//        pd($res);
//    }
}