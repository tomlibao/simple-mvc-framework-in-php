<?php
namespace app\models;

use core\base\Model;

class School extends Model
{
    protected $tables = 'school';

    public function __construct()
    {
        parent::__construct($this->tables);
    }

    public function hasManyClass()
    {
        return $this->hasManyThrough(Classroom::class, Student::class, 'id', 'school_id', 'cid');
    }

//    public function getAddressAttr($cname)
//    {
////        return $cname;
//        return '学校';
//    }
}