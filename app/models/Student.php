<?php
namespace app\models;

use core\base\Model;

class Student extends Model
{
    protected $tables = 'student';

    public function __construct()
    {
        parent::__construct($this->tables);
    }

    public function hasOneClassroom()
    {
        return $this->hasOne(Classroom::class, 'id', 'cid');
    }



}