<?php

namespace app\validate;

use core\validate\Validate;

class Student extends Validate
{
    protected $rule = [
        'name'  => 'require|number|email',
        'age'   => 'number|between:888,999',
    ];

    protected $message = [
        'name.require' => '名称不能为空',
        'name.number'     => '名称必须是数字',
        'name.email'     => '名称不是邮箱格式',
        'age.number'   => '年龄必须是数字',
        'age.between'  => '年龄必须在888~999之间',
    ];

    protected $scene = [
        'edit' => ['age'],
        'add' => ['name', 'age']
    ];
}