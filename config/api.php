<?php
use core\route\Route;

//Route::any('/school', 'School/demo');
Route::any('/v1/talk/test', 'v1/Talk/test');
//Route::any('/v1/demo', 'v1/Demo/demo');
//Route::any('/v1/test/test', 'v1/Test/test')->afterMiddleware(new \app\middleware\CheckUser());
//Route::any('/v1/demos', 'v1/Demo/demos');
//Route::any('/v1/demo', 'v1/Demo/demo')->afterMiddleware(new \app\middleware\CheckUser());
//Route::any('/v1/demo', function (){
//    echo '我是路由闭包！';
//});
//Route::any('/v1demo', 'v1/Test/demo');
//Route::any('/haha', 'v1/Test/haha');
//Route::any('/v1/response', 'v1/Test/response');
//Route::any('/v1/auth', 'v1/Test/auth');
//Route::any('/v1/validate', 'v1/Test/validate');
//Route::get('/v1/test/test', 'v1/Test/test');
//Route::any('/v1/test/test', 'v1/Demo/demo');
//Route::group("/v1", function (){
//    Route::get('/route1', 'v1/Demo/route1');
//    Route::get('/test/test', function (){
//        return reJson(200, '哈哈', [1, 2, 3]);
//    });
//    Route::get('/route2', 'v1/Demo/route2');
//});

//Route::group('/v1', function (){
//    Route::any('/demo', 'v1/Demo/demo');
//});