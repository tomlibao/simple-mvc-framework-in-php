<?php

namespace core\auth;

class Cookie
{
    /**
     * 设置 cookie
     * @param $key
     * @param $value
     * @return bool
     */
    public static function set($key, $value)
    {
        return setcookie($key, $value);
    }

    /**
     * 获取 cookie
     * @param string $key
     * @return array|false|mixed
     */
    public static function get($key = '')
    {
        if (empty($key)){
            return $_COOKIE;
        }else{
            return isset($_COOKIE[$key])?$_COOKIE[$key] : false;
        }
    }

    /**
     * 删除 cookie
     * @param $key
     */
    public static function delete($key)
    {
        unset($_COOKIE[$key]);
    }
}