<?php
namespace core\auth;

class Session
{
    /**
     * 获取 session
     * @param string $field
     * @return array|mixed
     */
    public static function get($field = '')
    {
        return empty($field)? $_SESSION : $_SESSION[$field];
    }

    /**
     * 设置 session
     * @param $field
     * @param $value
     * @return mixed
     */
    public static function set($field, $value)
    {
        return $_SESSION[$field] = $value;
    }

    /**
     * 删除 session
     * @param $field
     */
    public static function delete($field)
    {
        unset($_SESSION[$field]);
    }

    /**
     * 清空 session
     * @return array
     */
    public static function clear()
    {
        return $_SESSION = [];
    }
}