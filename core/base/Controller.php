<?php
namespace core\base;

use core\request\Request;
use ReflectionClass;

class Controller
{
    protected $view;

    protected $request;

    public function __construct($controller='', $action='')
    {
        //这个判断是为了防止不通过URL，控制器之间的互调
//        if (empty($controller)||empty($action)){
//            $class = new ReflectionClass($this);
//            $properties = $class->getMethods();
//            $nowControllerArr = json_decode(json_encode($properties),true);
//            $action = $nowControllerArr[0]['name'];
//            $controllerArr = explode("\\",$nowControllerArr[0]['class']);
//            $controller = str_replace('Controller','',$controllerArr[2]);
//        }
        $this->view = new View($controller, $action);
        $this->request = new Request();
    }

    public function assign($name, $value)
    {
        $this->view->assign($name, $value);
    }

    public function render()
    {
        $this->view->render();
    }
}