<?php

namespace core\base;

use core\request\Request;

interface Middleware
{
    public function handle(Request $request);
}