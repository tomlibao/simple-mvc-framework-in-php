<?php
namespace core\base;

use core\db\Orm;

class Model extends Orm
{

    public function __construct($table)
    {
        parent::__construct($table);
    }

}