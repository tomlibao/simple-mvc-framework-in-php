<?php
namespace core\exception;

class BaseException
{
    /**
     * 应用程序初始化
     */
    public function __construct() {
        /**
         * 自定义错误处理
         * @access public
         * @param int $errno 错误类型
         * @param string $errstr 错误信息
         * @param string $errfile 错误文件
         * @param int $errline 错误行数
         * @param int $errcontext 错误上下文
         * @return void
         */
        set_error_handler(function($errno, $errstr, $errfile, $errline, $errcontext){
            switch ($errno) {
                case E_ERROR:
                case E_PARSE:
                case E_CORE_ERROR:
                case E_COMPILE_ERROR:
                case E_USER_ERROR:
                    ob_end_clean();
                    $errorStr = "$errstr ".$errfile." 第 $errline 行.";
                    throw new \Exception($errorStr);
                    break;
                default:
                    $errorStr = "[$errno] $errstr ".$errfile." 第 $errline 行.";
                    throw new \Exception($errorStr);
                    break;
            }
            exit;
        });
    }

}