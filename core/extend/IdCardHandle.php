<?php
namespace core\extend;

class IdCardHandle
{
    /**
     * 通过身份号获取性别
     * @param $idCard:身份证号
     * @return int
     */
    public function getSexByIdCard($idCard)
    {
        //倒数第二位，单数为男，复数为女
        $sex = mb_substr($idCard,16,1);
        return $sex % 2 === 0 ? 1 : 2;
    }

    /**
     * 通过身份证获取生肖
     * @param $idCard:身份证号
     * @return string
     */
    public function getSXbyIdCard($idCard)
    {
        $start = 1901;
        $end = mb_substr($idCard,6,4);
        $x = ($start - $end) % 12;
        $value = "";
        if($x == 1 || $x == -11){$value = "鼠";}
        if($x == 0) {$value = "牛";}
        if($x == 11 || $x == -1){$value = "虎";}
        if($x == 10 || $x == -2){$value = "兔";}
        if($x == 9 || $x == -3){$value = "龙";}
        if($x == 8 || $x == -4){$value = "蛇";}
        if($x == 7 || $x == -5){$value = "马";}
        if($x == 6 || $x == -6){$value = "羊";}
        if($x == 5 || $x == -7){$value = "猴";}
        if($x == 4 || $x == -8){$value = "鸡";}
        if($x == 3 || $x == -9){$value = "狗";}
        if($x == 2 || $x == -10){$value = "猪";}
        return $value;
    }

    /**
     * 通过身份证号判断是否是未成年 true是未成年 false不是未成年
     * @param $idCard
     * @return bool
     */
    public function getIsUnderAge($idCard)
    {
        $idCardYear = mb_substr($idCard, 6, 4);
        $nowYear = date('Y');
        $age = $nowYear - $idCardYear;
        if ($age < 18){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 通过身份证号获取年龄
     * @param $idCard
     * @return false|string
     */
    public function getAgeByIdCard($idCard)
    {
        $idCardYear = mb_substr($idCard, 6, 4);
        $nowYear = date('Y');
        return $nowYear - $idCardYear;
    }

    /**
     * 通过身份证号获取星座
     * @param $idCard
     * @return string
     */
    public function getXingZuoByIdCar($idCard)
    {
        $bir = mb_substr($idCard,10,4);
        $month = (int)mb_substr($bir,0,2);
        $day = (int)mb_substr($bir,2);
        $strValue = '';
        if (($month == 1 && $day >= 20) || ($month == 2 && $day <= 18)) {
            $strValue = "水瓶座";
        } else if (($month == 2 && $day >= 19) || ($month == 3 && $day <= 20)) {
            $strValue = "双鱼座";
        } else if (($month == 3 && $day > 20) || ($month == 4 && $day <= 19)) {
            $strValue = "白羊座";
        } else if (($month == 4 && $day >= 20) || ($month == 5 && $day <= 20)) {
            $strValue = "金牛座";
        } else if (($month == 5 && $day >= 21) || ($month == 6 && $day <= 21)) {
            $strValue = "双子座";
        } else if (($month == 6 && $day > 21) || ($month == 7 && $day <= 22)) {
            $strValue = "巨蟹座";
        } else if (($month == 7 && $day > 22) || ($month == 8 && $day <= 22)) {
            $strValue = "狮子座";
        } else if (($month == 8 && $day >= 23) || ($month == 9 && $day <= 22)) {
            $strValue = "处女座";
        } else if (($month == 9 && $day >= 23) || ($month == 10 && $day <= 23)) {
            $strValue = "天秤座";
        } else if (($month == 10 && $day > 23) || ($month == 11 && $day <= 22)) {
            $strValue = "天蝎座";
        } else if (($month == 11 && $day > 22) || ($month == 12 && $day <= 21)) {
            $strValue = "射手座";
        } else if (($month == 12 && $day > 21) || ($month == 1 && $day <= 19)) {
            $strValue = "魔羯座";
        }
        return $strValue;
    }
}