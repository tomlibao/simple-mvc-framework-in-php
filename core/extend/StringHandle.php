<?php
namespace core\extend;

class StringHandle
{
    /**
     * 功能：判断一个字符串是否包含某些字符
     * @author tomluckin
     * @param $string:完整的字符串
     * @param $content:字符串内容
     * @return bool
     */
    public function isContain($string, $content)
    {
        if (strpos($string, $content) !== false){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 功能：判断一个字符串是否全部都是中文
     * @param $string:目标字符串
     * @return bool
     */
    public function isAllContainChinese($string)
    {
        if (preg_match('/^[\x{4e00}-\x{9fa5}]+$/u', $string)>0){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 功能：判断字符串中是否包含中文
     * @param $string:目标字符串
     * @return bool
     */
    public function isContainChinese($string)
    {
        if (preg_match("/[\x7f-\xff]/", $string)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 功能：是否包含合法字符串（数字、字母、下划线）
     * @param $string:目标字符串
     * @return bool
     */
    public function isLegalString($string)
    {
        if (preg_match('/^[A-Za-z0-9_]+$/u',$string)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 功能：获取指定长度的随机字符串
     * @param $length 要获取的字符串的长度
     * @return string
     */
    public function getRandStrByLength($length)
    {
        $str =  sprintf(
                '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0x0fff) | 0x4000,
                mt_rand(0, 0x3fff) | 0x8000,
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff)
            ).sprintf(
                '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0x0fff) | 0x4000,
                mt_rand(0, 0x3fff) | 0x8000,
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff),
                mt_rand(0, 0xffff));
        $str = str_replace('-','',$str);
        return mb_substr($str,0,$length);
    }

    /**
     * 功能：去除数字字符串中的前导 0
     * @param $string 目标字符串
     * @return string
     */
    public function getRemoveHeadZero($string)
    {
        return ltrim($string, '0');
    }

    /**
     * 去除字符串最后一位
     * @param $string
     * @return string
     */
    public function removeStringLast($string)
    {
        return mb_substr($string, 0, -1);
    }

    /**
     * 去除字符串第一位
     * @param $string
     * @return string
     */
    public function removeStringStart($string)
    {
        return mb_substr($string, 1);
    }

    /**
     * 功能：获取文件名后缀
     * @param $fileName 文件名
     * @return mixed|string
     */
    public function getFileNameSuffix($fileName)
    {
        return pathinfo($fileName)['extension'];
    }

    /**
     * 替换指定位数的字符串
     * @param $string 目标字符串
     * @param string $sign 要替换的符号
     * @param int $start 开始位置
     * @param int $length 从开始位置计算，要替换掉的数量
     * @return array|string|string[]
     */
    public function hideSignString($string, $sign = '*', $start = 0, $length = 1)
    {
        $handleString = $string;
        //获取目标字符串
        $targetString = mb_substr($handleString, $start, $length);
        //拼接指定位数的符号
        $str = "";
        for ($i = mb_strlen($targetString); $i--;){
            $str .= $sign;
        }
        //进行替换
        return str_replace($targetString, $str, $string);
    }
}