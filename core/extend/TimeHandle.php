<?php
namespace core\extend;

class TimeHandle
{
    /**
     * 根据日期获取是周几
     * @param string $time 时间戳
     * @return string
     */
    public function getWeekDate($time = '')
    {
        $week = ['周日','周一','周二','周三','周四','周五','周六'];
        return $week[date("w", !empty($time)? $time: time())];
    }

    /**
     * 计算两个时间相差的天数
     * @param $startTime 起始时间戳
     * @param $endTime 结尾时间戳
     * @return false|float
     */
    public function getDifferDayNumber($startTime, $endTime)
    {
        if ($startTime > $endTime){
            list($startTime, $endTime) = [$endTime, $startTime];
        }
        $differ = $endTime - $startTime;
        return floor($differ/86400);
    }

    /**
     * 计算某个日期，是否在某段时间之内
     * @param $start_date 开始时间
     * @param $end_date 结束时间
     * @param $check_date 要检查的时间
     */
    public function isExitBetweenTime($start_date, $end_date, $check_date)
    {
        // 定义开始和结束日期
        if (!is_numeric($start_date) && !is_numeric($end_date)){
            $start_date = new DateTime($start_date);
            $end_date = new DateTime($end_date);
        }
        // 要检查的日期
        if (!is_numeric($check_date)){
            $check_date = new DateTime($check_date);
        }

        // 判断是否在开始和结束日期之间
        if ($check_date >= $start_date && $check_date <= $end_date) {
            return true;
        } else {
            return false;
        }
    }
}