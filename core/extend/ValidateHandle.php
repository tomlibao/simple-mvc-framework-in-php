<?php
namespace core\extend;

class ValidateHandle
{
    /**
     * 验证手机号是否合法
     * @describe 合法返回true，不合法返回false
     * @param $phone:手机号
     * @return bool
     */
    public function isLegalPhone($phone)
    {
        if(!preg_match("/^1[345678]{1}\d{9}$/",$phone))
        {
            return false;
        }
        return true;
    }

    /**
     * 验证邮箱是否合法
     * @describe 合法返回true，不合法返回false
     * @param $email:邮箱地址
     * @return bool
     */
    public function isLegalMail($email)
    {
        $para = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*\.)+[a-zA-Z]*)$/u";
        if(preg_match($para, $email)){
            return true;
        }else{
            return false;
        }
    }

    /**
     * 验证固定电话是否合法
     * @describe 合法返回true，不合法返回false
     * @param $telePhone:固定电话
     * @return bool
     */
    public function isLegalTelephone($telePhone)
    {
        if (!preg_match("/^([0-9]{3,4}-)?[0-9]{7,8}$/", $telePhone))
        {
            return false;
        }
        return true;
    }

    /**
     * 是否是个合法的URL
     * @describe 合法返回true，不合法返回false
     * @param $url
     * @return bool
     */
    public function isLegalUrl($url)
    {
        $r = "/http[s]?:\/\/[\w.]+[\w\/]*[\w.]*\??[\w=&\+\%]*/is";
        if (preg_match($r, $url)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 判断是否是合法IP
     * @param $ip
     * @return  bool
     */
    public function isLegalIp($ip) {
        if (preg_match('/^((?:(?:25[0-5]|2[0-4]\d|((1\d{2})|([1-9]?\d)))\.){3}(?:25[0-5]|2[0-4]\d|((1\d{2})|([1 -9]?\d))))$/', $ip)) {
            return true;
        } else {
            return false;
        }
    }

}