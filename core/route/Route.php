<?php
namespace core\route;

class Route
{
    //所有注册路由信息
    public static $info = [];

    private static $instance;

    //注册过中间件的路由
    private static $middleware = [];

    /**
     * get方式请求路由
     * @param $url
     * @param $controller
     * @return Route
     */
    public static function get($url , $controller)
    {
        if(!(self::$instance instanceof self))
        {
            self::$instance = new self();
        }

        $method = 'get';
        self::handleRoute($url , $controller, $method);

        return self::$instance;
    }

    /**
     * post方式请求路由
     * @param $url
     * @param $controller
     * @return Route
     */
    public static function post($url , $controller)
    {
        if(!(self::$instance instanceof self))
        {
            self::$instance = new self();
        }

        $method = 'post';
        self::handleRoute($url , $controller, $method);

        return self::$instance;
    }

    /**
     * 接受任何请求的路由
     * @param $url
     * @param $controller
     * @return Route
     */
    public static function any($url , $controller)  
    {
        if(!(self::$instance instanceof self))
        {
            self::$instance = new self();
        }
        $method = 'any';
        self::handleRoute($url , $controller, $method);

        return self::$instance;
    }

    /**
     * 路由处理
     * @param $url
     * @param $controller
     * @param $method
     */
    private static function handleRoute($url , $controller, $method)
    {
        $arr = [];
        $arr['url'] = $url;
        $arr['controller'] = $controller;
        $arr['method'] = $method;
        if (is_object($controller)){
            $arr['is_closure'] = true;
        }else{
            $arr['is_closure'] = false;
        }
        self::$info[] = $arr;
        self::$middleware['url'] = $url;
    }

    /**
     * 路由分组
     * @param $url
     * @param $func
     * @return Route
     */
    public static function group($url, $func)
    {
        $oldKey = array_keys(self::$info);
        $func();
        foreach (self::$info as $k => $v){
            if (!in_array($k, $oldKey)){
                $v['url'] = $url.$v['url'];
                $v['prefix'] = $url;
                $v['is_group'] = 1;
                self::$info[$k] = $v;
            }
        }
    }

    /**
     * 前置中间件
     * @param $class
     */
    public function beforeMiddleware($class)
    {
        $this->handleMiddleware($class, 'before_middleware');
    }

    /**
     * 后置中间件
     * @param $class
     */
    public function afterMiddleware($class)
    {
        $this->handleMiddleware($class, 'after_middleware');
     }

    /**
     * 中间件路由处理
     * @param $class
     * @param $behavior
     */
    private function handleMiddleware($class, $behavior)
    {
        $prefix = '';
        foreach (self::$info as $k => $v){
            if (isset($v['is_group'])){
                $middlewareUrl = $v['prefix'].self::$middleware['url'];
            }else{
                $middlewareUrl = self::$middleware['url'];
            }
            if ($v['url'] == $middlewareUrl){
                $prefix = isset($v['prefix']) ? $v['prefix'] : '';
                $v[$behavior] = $class;
                self::$info[$k] = $v;
            }
        }
        if ($prefix){
            foreach (self::$info as $k => $v){
                if (isset($v['prefix']) && $v['prefix'] == $prefix){
                    $v[$behavior] = $class;
                    self::$info[$k] = $v;
                }
            }
        }
        unset(self::$middleware['url']);
     }
}