<?php
namespace core\validate;

class Validate
{
    protected $rule = [];
    protected $message = [];
    protected $scene = [];
    protected $sceneName;
    protected $batch;

    protected static $returnMessage = [];

    /**
     * 处理校验的数据
     * @param $data
     * @return array|void
     */
    public function check($data)
    {
        $data = $this->handleData($data);
        $this->batch = '';
        $this->sceneName = '';
        return $data;
    }

    /**
     * 场景赋值
     * @param $name
     * @return $this
     */
    public function scene($name){
        $this->sceneName = $name;
        return $this;
    }

    /**
     * 是否开启批量验证
     * @param $bool
     * @return $this
     */
    public function batch($bool)
    {
        $this->batch = $bool;
        return $this;
    }

    /**
     * 实际数据校验方法
     * @param $data
     * @return array|void
     */
    private function handleData($data)
    {
        foreach ($this->rule as $k => $v){
            //验证是否在场景中
            if (isset($this->scene[$this->sceneName])){
                $sceneArr = $this->scene[$this->sceneName];
                if (!in_array($k, $sceneArr)){
                    continue;
                }
            }
            $arr = explode('|', $v);
            foreach ($arr as $value){
                $res = false;
                if(($value == 'require')){
                    //判断是否是必填
                    $res = $this->requireS($data, $k);
                } elseif ($value == 'number'){
                     //判断是否是数字
                    $res = $this->numberS($data, $k);
                }elseif ($value == 'email'){
                    //判断是否是邮箱
                    $res = $this->emailS($data, $k);
                }elseif (mb_substr($value, 0, 4) == 'max:'){
                    //最大值
                    $value = str_replace('max:', '', $value);
                    $res = $this->maxS($data, $k, $value);
                }elseif (mb_substr($value, 0, 4) == 'min:'){
                    //最小值
                    $value = str_replace('min:', '', $value);
                    $res = $this->minS($data, $k, $value);
                }elseif (mb_substr($value, 0, 8) == 'between:'){
                    //范围
                    $value = str_replace('between:', '', $value);
                    $arr = explode(',', $value);
                    $res = $this->betweenS($data, $k, $arr[0], $arr[1]);
                }
                if ($res && $this->batch != true){
                    return self::$returnMessage;
                }
            }
        }
        return self::$returnMessage;
    }

    /**
     * 必填字段验证
     * @param $data
     * @param $field
     * @return bool
     */
    private function requireS($data, $field)
    {
        if (!isset($data[$field]) || empty($data[$field])){
            $this->setReturnMessage($field, 'require');
            return true;
        }
        return false;
    }

    /**
     * 手机验证
     * @param $data
     * @param $field
     * @return bool
     */
    private function numberS($data, $field){
        if (!isset($data[$field]) || !is_numeric($data[$field])){
            $this->setReturnMessage($field, 'number');
            return true;
        }
        return false;
    }

    /**
     * 邮箱验证
     * @param $data
     * @param $field
     * @return bool
     */
    private function emailS($data, $field){
        $para = "/^([0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*@([0-9a-zA-Z][-\w]*\.)+[a-zA-Z]*)$/u";
        if(!isset($data[$field]) || !preg_match($para, $data[$field])){
            $this->setReturnMessage($field, 'email');
            return true;
        }else{
            return false;
        }
    }

    /**
     * 最小值验证
     * @param $data
     * @param $field
     * @param $value
     * @return bool
     */
    private function minS($data, $field, $value){
        if (!isset($data[$field]) || !is_numeric($data[$field]) || $data[$field] < $value){
            $this->setReturnMessage($field, 'min');
            return true;
        }else{
            return false;
        }
    }

    /**
     * 最大值验证
     * @param $data
     * @param $field
     * @param $value
     * @return bool
     */
    private function maxS($data, $field, $value){
        if (!isset($data[$field]) || !is_numeric($data[$field]) || $data[$field] > $value){
            $this->setReturnMessage($field, 'max');
            return true;
        }else{
            return false;
        }
    }

    /**
     * 范围验证
     * @param $data
     * @param $field
     * @param $start
     * @param $end
     * @return bool
     */
    private function betweenS($data, $field, $start, $end){
        if (!isset($data[$field]) || mb_strlen($data[$field]) < $start || mb_strlen($data[$field]) > $end){
            $this->setReturnMessage($field, 'between');
            return true;
        }else{
            return false;
        }
    }

    /**
     * 保存报错信息
     * @param $field
     * @param $funcName
     */
    public function setReturnMessage($field, $funcName)
    {
        if (isset($this->message[$field.'.'.$funcName]) && !empty($this->message[$field.'.'.$funcName])){
            self::$returnMessage[$field][] = $this->message[$field.'.'.$funcName];
        }else{
            self::$returnMessage[$field][] = $funcName;
        }
    }
}