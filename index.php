<?php
//定义应用目录
const APP_PATH = __DIR__ . '/';

//加载框架核心文件
require(APP_PATH . 'core/Core.php');

//注册自动加载
spl_autoload_register(array(new core\Core(), 'loadClass'));

//注册路由文件
require 'config/api.php';

//加载组件包
//require "vendor/autoload.php";

//加载公共文件
require 'core/common/common.php';

//实例化框架类并运行
(new core\Core())->run();