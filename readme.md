# 一、框架介绍

PHP初级就能看懂的框架

# 二、版本更新日志

### V1.1.1 更新时间：2021.06.13


框架基础版上线


### V1.1.2 更新时间：2021.07.25

新增：

- 新增Db类

修复：

- 修复一个控制器中不能调用另一个控制器

### V1.1.3 更新时间：2021.08.15

新增：

- 新增路由访问，能通过多层路径进行访问，例如：xx.com/v1/student/get

### V1.1.4 更新时间：2022.04.10

新增：

- db链式操作 find、update、insertGetLastId、setInc、setDec、clearParam 方法

修复：

- 一个方法中使用多次db查询的时候，上一个db条件没有被清空。

### V1.1.5 更新时间：2022.04.21

新增：

- pd 公共方法：print_r调试函数
- vd 公共方法：var_dump调试函数
- reJson 公共方法：接口公共返回参数

修复：

- Db 的 where 条件，当多个 where 条件的时候最后的一个 where 条件会覆盖第一个。

调整：

- 将 404.html 从 core 目录更换到 static 目录。

### V1.1.6 更新时间：2022.04.28

新增：

- Db类 max、min、avg、value、execute、query 方法。

调整：

- 将 Request 类中的 get 和 post 方法中的三目运算符由 PHP7版本调整为 PHP5版本的。


### V1.1.7 更新时间：2022.05.27

新增：

- Route类 get、post、any、group、afterMiddleware、beforeMiddleware 方法

### V1.1.8 更新时间：2022.05.30

新增：

- Orm类：实现了模型的增删改查，以及模型的关联查询

### V1.1.9 更新时间：2022.05.31

新增：

- validate：实现了验证器验证，验证场景、验证规则

### V1.2.1 更新时间：2022.10.23

修复：

- Db和orm类：修复Db类中使用find查询，未查询出数据返回字符串NULL的问题


