# 一、并发场景下事务的数据问题


# 二、MySQL的四种隔离级别

### 2.1 脏读

事务A 读取了 事务B 为提交的数据，事务B 回滚，那么 事务A 读的就是脏数据

### 2.2 不可重复读

事务A 读取 同一数据，事务B 在这个过程中进行修改，导致 事务A 每次读的数据都不一致。

### 2.3 幻读

有一个学校，所有学生都打了疫苗，事务A 需要将所有学生都标注为已打疫苗状态。 事务B 这时候插入了一条未打疫苗的新生，事务A 发现数据没改完，和发生幻觉一样，这就是幻读。






https://blog.csdn.net/qq_39390545/article/details/107343711
https://www.cnblogs.com/wyaokai/p/10921323.html
# 三、隔离级别的修改和查看

### 3.1 查看当前会话的隔离级别

```sql
mysql>  select @@tx_isolation;
+-----------------+
| @@tx_isolation  |
+-----------------+
| REPEATABLE-READ |
+-----------------+
1 row in set, 1 warning (0.00 sec)
```

### 3.2 查看当前系统会话的隔离级别

```sql
mysql> select @@global.tx_isolation;
+-----------------------+
| @@global.tx_isolation |
+-----------------------+
| REPEATABLE-READ       |
+-----------------------+
1 row in set, 1 warning (0.00 sec)
```

### 3.3 设置会话的隔离级别，隔离级别由低到高设置依次为

```sql
set session transaction isolation level read uncommitted;
set session transaction isolation level read committed;
set session transaction isolation level repeatable read;
set session transaction isolation level serializable;
```


### 3.4 设置当前系统的隔离级别，隔离级别由低到高设置依次为

```sql
set global transaction isolation level read uncommitted;
set global transaction isolation level read committed;
set global transaction isolation level repeatable read;
set global transaction isolation level serializable;
```



# 四、案例分析

### 4.1 读未提交（RU）


### 4.1 读提交（RC）


### 4.1 可重复读（RR）


### 4.1 串行化（RU）